Product.destroy_all
Category.destroy_all

# Seeding the various categories
puts '------------------------------------------------'
puts 'Seeding Categories...'
puts '------------------------------------------------'
['makeup', 'brushes', 'fragrance', 'hair', 'skincare'].each do |product|
  unless Category.exists?(name: product)
    print "Seeding category #{product}..."
    Category.create!(name: product)
    puts 'Done!'
  end
end

#Seeding various products associated to categories
puts '------------------------------------------------'
puts 'Seeding Products...!'
puts '------------------------------------------------'
puts 'Seeding Make Up Products...!'
puts '------------------------------------------------'
makeup = Category.find_by_name('makeup')
prices = [1010, 1220, 2080, 6040, 1700, 2300, 4500, 2200,3000, 5580, 5530, 13300, 550000, 250000 ]
sale_text = ["50% OFF", "30% OFF" ]
image_links = [
  'https://bit.ly/2JnQtxE',
  'https://bit.ly/2vHji6f',
  'https://bit.ly/2FcqVkW',
  'https://static-reg.lximg.com/images/pictures/38056/zoom_5861c5d1b4f7c4b8f0958ebf0730fbe2faabdc82_1493190637_4368_ZOEVA_WEB.jpg',
  'https://static-reg.lximg.com/images/product_images/closeup_17445_Nudestix_WEB_b22aaa9c200da8ba2c290ba9f7859271c2c6d24b_1523777337.png',
  'https://static-reg.lximg.com/images/pictures/23773/closeup_473a6ad989f8f88f570da1083f4bf9ee4ac4950a_1493175048_MJB_SPOTLIGHT_GLOW_STICK_WEB.jpg',
  'https://static-reg.lximg.com/images/pictures/53004/closeup_ce0eea0714cadfbd8d499483d0415ffa737cfed1_1501749481_26137_Fenty_MatchStixMatteSkinstickAlmond_WEB.jpg',
  'https://static-reg.lximg.com/images/product_images/closeup_34248_TarteCosmetics_BeAMermaidMakeWavesEyeshadowPaletteLE_Default_WEB_d7c5f0ece65f2648943b11e4e5daa6ac0f42da56_1523780024.png',
  'https://static-reg.lximg.com/images/pictures/45300/closeup_70358adf2fa1c70ea1373f46c2e3f32269ad13c4_1493212219_19333_ITCosmetics_WEB.jpg',
  'https://static-reg.lximg.com/images/product_images/closeup_2763_3CE_WEB_cdc9cf345d1c18fe18e58866b4f90dd8b37cb66e_1523776547.png',
  'https://static-reg.lximg.com/images/pictures/45999/closeup_4a6c2d4cfee6d3ec416d4701897c066b92f738af_1493214563_6286_EsteeLauder_Web.jpg',
  'https://static-reg.lximg.com/images/product_images/closeup_35111_BenefitCosmetics_WEB_2_151aecf31a3214d49a2c597a8ed697bc2cf6618c_1523780492.png',
  'https://static-reg.lximg.com/images/pictures/54295/zoom_dd2908f2a91deda5bf685be25ff4337030f0c879_1505657129_29569_SephoraCollection_WEB_1.jpg',
  'https://static-reg.lximg.com/images/pictures/56199/closeup_55822369bc0215cfb41afb155edcadebdacf0d6f_1509004137_29626_NatashaDenona_StarEyeshadowPalette_WEB.jpg'
]

descriptions = [
  'Mother of All - Mini Lipstick Duo (Limited Edition)',
  'CC Cream',
  'Shimmering Skin Perfector Pressed Sananas Highlighter (Limited Edition)',
  'Naturally Yours Palette',
  'Intense Matte Lip & Cheek Pencil',
  'Glow Stick Glistening Illuminator',
  'Match Stix Matte Skinstick',
  'Be A Mermaid & Make Waves Eyeshadow Palette (Limited Edition)',
  'Your Skin But Better CC Cream SPF 50+',
  'Shimmer Stick',
  'Double Wear Stay-In-Place Makeup',
  'Gimme Brow +',
  'It Is Never Too Much! Lip Palette',
  'Star Eyeshadow Palette'
]

['Kat Von D Beauty', 'Sephora Cream', 'Becca', 'Zoeva', 'NUDESTIX', 'Marc Jacobs Beauty', 'Fenty Beauty', 'Tarte', 'IT Cosmetics', '3CE', 'ESTÉE LAUDER', 'BENEFIT COSMETICS', 'Sephora Collection', 'NATASHA DENONA'].each_with_index do |product, index|
  Product.create!(category: makeup,
                  name: product,
                  description: descriptions[index],
                  sold_out: false,
                  under_sale: false,
                  price: prices[index],
                  sale_price: prices[index],
                  sale_text: sale_text[index],
                  image_source: image_links[index]
                )
end

puts 'Seeding Brush Products...!'
puts '------------------------------------------------'

brushes = Category.find_by_name('brushes')
prices = [500, 1450, 1700]
image_links = [
  'https://bit.ly/2r0g34C',
  'https://bit.ly/2qXyruD',
  'https://bit.ly/2K94QaF'
]

descriptions = [
  'Rose Golden Complete Set Vol. 1',
  'Tarteist Toolbox Brush Set & Magnetic Palette',
  'F80 Flat Kabuki Brush'
]

['Zoeva', 'Tarte', 'Sigma Beauty'].each_with_index do |product, index|
  Product.create!(category: brushes,
                  name: product,
                  description: descriptions[index],
                  sold_out: false,
                  under_sale: false,
                  price: prices[index],
                  sale_price: prices[index],
                  sale_text: sale_text[index],
                  image_source: image_links[index]
                )
end

puts 'Seeding Perfume Products...!'
puts '------------------------------------------------'

fragrance = Category.find_by_name('fragrance')

image_links = [
  'https://bit.ly/2qU8L2U',
  'https://bit.ly/2qXyruD'
]

descriptions = [
  'Heart Chakra Aromatherapy Oil',
  'La Vie Est Belle Eau De Parfum',
]

['KORA ORGANICS BY MIRANDA KERR', "LANCÔME"].each_with_index do |product, index|
  Product.create!(category: fragrance,
                  name: product,
                  description: descriptions[index],
                  sold_out: false,
                  under_sale: false,
                  price: prices[index],
                  sale_price: prices[index],
                  sale_text: sale_text[index],
                  image_source: image_links[index]
                )
end

puts 'Seeding Hair Products...!'
puts '------------------------------------------------'

hair = Category.find_by_name('hair')

image_links = [
  'https://static-reg.lximg.com/images/pictures/50551/closeup_194b86f0f1f1918717d7beca59c87f01ab22fd7f_1494841715_26581_GlossModerne_WEB.jpg'
]

descriptions = [
  'Clean Luxury Shampoo + Conditioner Duo'
]

['GLOSS MODERNE'].each_with_index do |product, index|
  Product.create!(category: hair,
                  name: product,
                  description: descriptions[index],
                  sold_out: false,
                  under_sale: false,
                  price: prices[index],
                  sale_price: prices[index],
                  sale_text: sale_text[index],
                  image_source: image_links[index]
                )
end

puts 'Seeding Skincare Products...!'
puts '------------------------------------------------'


skincare = Category.find_by_name('skincare')

image_links = [
  'https://static-reg.lximg.com/images/product_images/closeup_Pixi_GlowTonic_15ml_407e76bd549674fa88c154fa843e661fcc6421f7_1523777781.png'
]

descriptions = [
  'Glow Tonic'
]

['PIXI'].each_with_index do |product, index|
  Product.create!(category: skincare,
                  name: product,
                  description: descriptions[index],
                  sold_out: false,
                  under_sale: false,
                  price: prices[index],
                  sale_price: prices[index],
                  sale_text: sale_text[index],
                  image_source: image_links[index]
                )
end

puts '------------------------------------------------'
puts 'Seeding Products Done!'
puts '------------------------------------------------'
