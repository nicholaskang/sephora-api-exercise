require 'rails_helper'

RSpec.describe Api::V1::CategoriesController, type: :controller do
    describe "GET #index" do
    it 'returns a success response' do
        expect(response).to have_http_status(200)
    end

    it 'returns all the categories' do
        create_list(:category, 10)
  
        get :index, format: :json
  
        parsed_response = JSON.parse(response.body)
  
        expect(parsed_response.length).to be 10
      end 
    end
end