require 'rails_helper'

RSpec.describe Api::V1::ProductsController, type: :controller do

  describe "GET #index" do
    it 'returns a success response' do
      expect(response).to have_http_status(200)
    end

    it 'returns all the products associated to a category' do
      category = create(:category)
      product = create(:product, {category_id: category.id})
      
      get :index, {category: category.name}, format: :json

      parsed_response = JSON.parse(response.body)
      parsed_response_ids = parsed_response.map{|el| el['id']}

      expect([parsed_response_ids]).to include(category.products.map(&:id))
      
    end

    it 'returns all the products' do
      create_list(:product, 10)

      get :index, format: :json

      parsed_response = JSON.parse(response.body)

      expect(parsed_response.length).to be 10
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      expect(response).to have_http_status(200)
    end

    it 'returns a specific product' do
      product = create(:product)

      get :show, id: product.id, format: :json
      parsed_response = JSON.parse(response.body)
      
      expect(parsed_response['id']).to eq(product.id)
    end

    it 'returns error message when product does not exist' do
      get :show, id: 90, format: :json
      parsed_response = JSON.parse(response.body)

      expect(response.status).to eq(404)
      expect(parsed_response['error']).to eq("Couldn't find Product with 'id'=90")
    end
  end

  describe '#sort_by_price' do
    let!(:category) { create(:category) }
    let!(:products_2500) { create_list(:product, 3, category: category) }
    let!(:products_4000) { create_list(:product, 3, category: category, sale_price: 4000) }

    context 'when category is present' do

      it 'assigns instance variable category' do
        params = {:category => 'perfume', 'price_filter' => '2000-3000'}
        allow(controller).to receive(:params).and_return(params)
        
        get :sort_by_price, format: :json

        expect(assigns(:category)).to eq(category)
      end
    end

    context 'when category is not present' do

      it 'does not assign instance variable category' do
        params = {:category => '', 'price_filter' => '2000-3000'}
        allow(controller).to receive(:params).and_return(params)
        
        get :sort_by_price, format: :json

        expect(assigns(:category)).to be_nil
      end
    end
    
    it 'retrieves products within a given price range' do
      params = {'category' => 'perfume', 'price_filter' => '2000-3000'}
      allow(controller).to receive(:params).and_return(params)
      
      get :sort_by_price, format: :json
      
      parsed_response = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(parsed_response.length).to be(3)
    end
  end
end