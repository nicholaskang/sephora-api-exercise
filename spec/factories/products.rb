FactoryBot.define do
  factory :product do
    association :category, factory: :category, strategy: :build

    name 'Mont Blanc Perfume'
    sold_out false
    under_sale false
    price  5000
    sale_price 2500
    sale_text "50% OFF"

    category_id nil
  end
end
