Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :categories, only: [:index]
      resources :products, only: [:index, :show] do
        collection do
          get 'sort_by_price'
        end
      end
    end
  end
end
