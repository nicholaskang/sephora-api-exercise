module Api

  module V1

    class ProductsController < ApplicationController

      before_action :find_category, only: [:index, :sort_by_price]

      def index
        if @category.present?
          @products = Product.where(category_id: @category.id)
        else
          @products = Product.all
        end
        render json: @products, status: :ok
      end

      def show
        begin
          render json: product, status: :ok
        rescue ActiveRecord::RecordNotFound => e
          render json: { error: e.to_s }, status: :not_found
        end
      end

      def sort_by_price
        products = (@category.nil? ? Product.all : @category.products)
        products = products.select do |product|
          product.sale_price.between?(price_filter.first - 1, price_filter.last + 1)
        end

        render json: products, status: :ok
      end

      private

      def find_category
        @category = Category.find_by_name(params[:category])
      end

      def product
        @product = Product.find(params[:id])
      end

      def price_filter
        params.fetch('price_filter').split('-').map(&:to_i)
      end

    end

  end

end
